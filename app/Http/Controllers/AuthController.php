<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    //
    use AuthenticatesUsers;

    public function _construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function sendLoginRequest(Request $request, User $user)
    {
        $email = $request->email;
        $pass = $request->password;

        $data = $user::where('email', $email)->first();
        if ($data) {
            if (Hash::check($pass, $data->password)) {
                Session::put('name', $data->name);
                Session::put('email', $data->email);
                Session::put('login', true);

                return redirect()->route('tampil_barang');
            } else {
                return redirect()->back()->with('error', 'Invalid Email Adress or Password');
            }
        } else {
            return redirect()->back()->with('error','No Data Found');
        }

    }

    public function logout(Request $request){
        if(\Auth::check()){
            \Auth::logout();
            $request->session()->invalidate();
        }

        return redirect()->route('login');
    }
}