<?php

namespace App\Http\Controllers;

use App\BarangModel;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    //
    public function index()
    {
        $data = BarangModel::all()->where('is_active', '1');

        // return view('master_barang.index', compact('data'));

        return view('admin.barang.index', compact('data'));
    }

    public function createData()
    {
        return view('master_barang.tambah');
    }

    public function postData(Request $request, BarangModel $barangModel)
    {
        $simpan = $barangModel->create([
            'nama_barang' => $request->nama_barang,
            'harga_satuan' => $request->harga_barang,
            'stok' => $request->stok_barang,
            'keterangan' => $request->keterangan,
            'id_suplier' => $request->id_suplier,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_barang')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('tampil_barang')->with('success', 'Data Berhasil Disimpan');
    }

    public function editData($id)
    {
        $data = BarangModel::where('kode_barang', $id)->first();

        return view('admin.barang.edit', compact('data'));
    }

    public function updateData($id, Request $request, BarangModel $barangModel)
    {
        $simpan = $barangModel->where('kode_barang', $id)->update([
            'nama_barang' => $request->nama_barang,
            'harga_satuan' => $request->harga_barang,
            'stok' => $request->stok_barang,
            'keterangan' => $request->keterangan,
            'is_active' => 1,
            'id_suplier' => $request->id_suplier,
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_barang')->with('error', 'Data Gagal di Update');
        }

        return redirect()->route('tampil_barang')->with('success', 'Data Berhasil di Update');
    }

    public function deleteData($id, BarangModel $barangModel)
    {
        $delete = $barangModel->where('kode_barang', $id)->update([
            'is_active' => '0',
        ]);

        if (!$delete) {
            return redirect()->route('tampil_barang')->with('error', 'Data Gagal di Update');
        }

        return redirect()->route('tampil_barang')->with('success', 'Data Berhasil di Update');
    }

    // public function deleteData($id, BarangModel $barangModel)
    // {
    //     $barangModel::where('kode_barang', $id)->delete();

    //     return redirect()->route('tampil_barang')->with('success', 'Data Berhasil Dihapus');
    // }

    // public function trash()
    // {
    //     $trash = BarangModel::onlyTrashed()->get();

    //     return view('master_barang.trash', ['trash' => $trash]);
    // }

    // public function restoreData($id)
    // {
    //     $restore = BarangModel::onlyTrashed()->where('kode_barang', $id)->restore();

    //     return redirect()->route('tampil_barang')->with('success', 'Data Berhasil Dikembalikan');
    // }

    // public function restoreAll()
    // {
    //     $restore = BarangModel::onlyTrashed()->restore();

    //     return redirect()->route('tampil_barang')->with('success', 'Semua Data Berhasil Dikembalikan');
    // }

    // public function permanenDelete($id)
    // {
    //     BarangModel::onlyTrashed()->where('kode_barang', $id)->forceDelete();

    //     return redirect()->route('trash')->with('success', 'Data Berhasil Dihapus Permanen');
    // }

    // public function deleteAll()
    // {
    //     $restore = BarangModel::onlyTrashed()->forceDelete();

    //     return redirect()->route('tampil_barang')->with('success', 'Semua Data di Tong Sampah Berhasil Dihapus');
    // }
}