<?php

namespace App\Http\Controllers;

use carbon;
use Illuminate\Http\Request;

class TugasController extends Controller
{
    //
    public function index()
    {
        return view('formtugas');
    }

    public function hitungberat(Request $request)
    {
        $jk = $request->input('jk');
        $tinggi = $request->tinggi;
        $berat = $request->berat;
        $wanita = "wanita";
        $pria = "pria";

        if ($jk == $wanita) {
            $bb = ($tinggi - 100) - (($tinggi - 100) * 15 / 100);
            ceil($bb);
            if ($berat == ceil($bb)) {
                return redirect('/tugas')->with('status', 'Selamat, BB anda proporsional || BB paling ideal anda ' . $bb . ' kg');
            } else {
                return redirect('/tugas')->with('status', 'Maaf, BB anda belum proporsional || BB ideal anda ' . $bb . ' kg');
            }
        } elseif ($jk == $pria) {
            $bb = ($tinggi - 100) - (($tinggi - 100) * 10 / 100);
            ceil($bb);
            if ($berat == ceil($bb)) {
                return redirect('/tugas')->with('status', 'Selamat, BB anda proporsional || BB paling ideal anda ' . $bb . ' kg');
            } else {
                return redirect('/tugas')->with('status', 'Maaf, BB anda belum proporsional || BB ideal anda ' . $bb . ' kg');
            }
        };
    }

    public function hitungumur(Request $request)
    {
        $tgl_lahir = $request->input('tgl_lahir');
        $now = carbon\carbon::now();
        $b_day = carbon\carbon::parse($tgl_lahir);
        $age = $b_day->diffInYears($now);

        $a = $age;

        switch ($a) {
            case (($a >= 0) and ($a <= 5)):
                return redirect('/tugas')->with('statusdua', 'Usia anda adalah: ' . $age . ' tahun || Anda Masih Balita ');
                break;
            case (($a >= 6) and ($a <= 16)):
                return redirect('/tugas')->with('statusdua', 'Usia anda adalah: ' . $age . ' tahun || Anda Masih Anak-Anak ');
                break;
            case (($a >= 17) and ($a <= 50)):
                return redirect('/tugas')->with('statusdua', 'Usia anda adalah: ' . $age . ' tahun || Anda Sudah Dewasa ');
                break;
            case ($a > 50):
                return redirect('/tugas')->with('statusdua', 'Usia anda adalah: ' . $age . ' tahun || Anda Sudah Tua ');
                break;
        }

    }
}