<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data Barang</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="card mt-5 align-items-center">
            <div class="card-body col-5">
                <h2 class="text-center text-info p-2"><u>Tambah Data Barang</u></h2><br>
                <form action="{{ route('post_barang') }}" method="post">
                    @csrf
                    <div class="form-group col-12">
                        Nama Barang<br>
                        <input class="form-control bg-gray-100 mt-2" type="text" name="nama_barang"
                            id="nama_barang"><br><br>
                        Harga Barang<br>
                        <input class="form-control bg-gray-100 mt-2" type="number" name="harga_barang"
                            id="harga_barang"><br><br>
                        Stok<br>
                        <input class="form-control bg-gray-100 mt-2" type="number" name="stok_barang"
                            id="stok_barang"><br><br>
                        Keterangan<br>
                        <input class="form-control bg-gray-100 mt-2" type="text" name="keterangan"
                            id="keterangan"><br><br>
                        Id Suplier<br>
                        <input class="form-control bg-gray-100 mt-2" type="text" name="id_suplier"
                            id="id_suplier"><br><br>

                        <input class="btn btn-info btn-sm" type="button" value="Kembali">
                        <input class="btn btn-success btn-sm" type="submit" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>