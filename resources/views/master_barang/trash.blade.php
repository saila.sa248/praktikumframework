<!DOCTYPE html>
<html>

<head>
    <title>Tong Sampah</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>

<body>

    <div class="container">

        <div class="card mt-5">
            <div class="card-body">

                <a href="{{ route('tampil_barang') }}">Data Barang</a>
                |
                <a href="">Tong Sampah</a>

                <br />
                <br />
                <a href="{{ route('restore_all') }}">Kembalikan Semua</a>
                |
                <a href="{{ route('delete_all') }}">Hapus Permanen Semua</a>
                <br />
                <br />

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Stok</th>
                            <th>Keterangan</th>
                            <th width="30%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($trash as $t)
                        <tr>
                            <td>{{ $t->kode_barang }}</td>
                            <td>{{ $t->nama_barang }}</td>
                            <td>{{ $t->harga_satuan }}</td>
                            <td>{{ $t->stok }}</td>
                            <td>{{ $t->keterangan }}</td>
                            <td>
                                <a href="{{ route('restore_data', $t->kode_barang) }}"
                                    class="btn btn-success btn-sm">Restore</a>
                                <a href="{{ route('hapus_permanen', $t->kode_barang) }}"
                                    class="btn btn-danger btn-sm">Hapus
                                    Permanen</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @if(session()->has('error'))
                {{session()->get('error')}}
                @endif
                @if(session()->has('success'))
                {{session()->get('success')}}
                @endif
            </div>
        </div>
    </div>

</body>

</html>