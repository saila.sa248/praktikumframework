<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use App\Mahasiswa;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fromview', function () {
    return view('hello');
});

// return Model
Route::get('/frommodel', function () {
    $data = Mahasiswa::all();
    return $data;
});

Route::get('/login', 'AuthController@index')->name('login_form');
Route::post('/signin', 'AuthController@sendLoginRequest')->name('ceklogin');
Route::get('/logout','AuthController@logout')->name('logout_action');

// Route::group(['middleware' => 'auth'], function(){

Route::get('/kalkulator', 'KalkulatorController@index');
Route::post('/proses', 'KalkulatorController@tambah')->name('tambah');
Route::post('/proseskali', 'KalkulatorController@kali')->name('kali');
Route::post('/prosesbagi', 'KalkulatorController@bagi')->name('bagi');
Route::post('/proseskurang', 'KalkulatorController@kurang')->name('kurang');

//Route Barang
Route::get('barang', 'BarangController@index')->name('tampil_barang');
Route::get('barang/create', 'BarangController@createData')->name('create_barang');
Route::post('barang/post', 'BarangController@postData')->name('post_barang');
Route::get('barang/edit/{id}', 'BarangController@editData')->name('edit_data');
Route::post('barang/update/{id}', 'BarangController@updateData')->name('update_data');
Route::get('barang/delete/{id}', 'BarangController@deleteData')->name('delete_data');
Route::get('/barang/trash', 'BarangController@trash')->name('trash');
Route::get('/barang/restore/{id}', 'BarangController@restoreData')->name('restore_data');
Route::get('/barang/restoreAll', 'BarangController@restoreAll')->name('restore_all');
Route::get('/barang/hapus/{id}', 'BarangController@permanenDelete')->name('hapus_permanen');
Route::get('/barang/deleteAll', 'BarangController@deleteAll')->name('delete_all');
Auth::routes();

// });

Route::get('/home', 'HomeController@index')->name('home');

// Route Tugas
Route::get('/tugas', 'TugasController@index');
Route::post('/bb', 'TugasController@hitungberat')->name('hitungbb');
Route::post('/age', 'TugasController@hitungumur')->name('age');